package com.declarativesystems.bitbucket.commitscanner.api;

public interface MyPluginComponent
{
    String getName();
}