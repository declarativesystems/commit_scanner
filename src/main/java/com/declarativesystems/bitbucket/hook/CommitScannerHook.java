package com.declarativesystems.bitbucket.hook;

import com.atlassian.bitbucket.comment.CommentThread;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.content.*;
import com.atlassian.bitbucket.hook.*;
import com.atlassian.bitbucket.hook.repository.*;
import com.atlassian.bitbucket.repository.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageProvider;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PagedIterable;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.StringOutputHandler;
import com.atlassian.utils.process.Watchdog;


@Named
public class CommitScannerHook implements PreRepositoryHook<RepositoryPushHookRequest>
{

//    @Override
//    public void onEnd(@Nonnull PreRepositoryHookContext context, @Nonnull RepositoryPushActivity request, @Nonnull RepositoryHookResult result) {
//
//    }
    private CommitService commitService;
    private GitCommandBuilderFactory gitCommandBuilderFactory;


    @Inject
    public CommitScannerHook(@ComponentImport CommitService commitService, @ComponentImport GitCommandBuilderFactory gitCommandBuilderFactory) {
        this.commitService = commitService;
        this.gitCommandBuilderFactory = gitCommandBuilderFactory;
    }


    //https://community.atlassian.com/t5/Answers-Developer-Questions/git-shortlog/qaq-p/527213
    public ArrayList<String> getItInParts(Repository repo, String commitId) {
        CommandOutputHandler outputHandler = new CommandOutputHandler() {

            ArrayList<String> errors = new ArrayList<>();
            @Override
            public void process(InputStream inputStream) throws ProcessException {


                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                String[] banned = {
                        ".*password.*",
                        ".*12345678.*",
                        ".*aaaaaaaa.*",
                        ".*changeme.*",
                        ".*football.*",
                };


                try {
                    int count = 0;
                    while ((line = br.readLine()) !=null) {


                        if (line.startsWith("+")) {
                            System.out.print(".");
                            if (count % 60 == 0) {
                                System.out.print("\n");
                            }
                            // new code added
                            for (String re : banned) {
                                System.out.println("compare "+re+" to "+line);
                                if (line.matches(re)) {
                                    errors.add("BANNED ---> " + line);
                                }
                            }
                            count++;
                        }
                    }
                } catch (IOException e){}
            }

            @Override
            public void complete() throws ProcessException {

            }

            @Override
            public void setWatchdog(Watchdog watchdog) {

            }

            @Nullable
            @Override
            public Object getOutput() {
                return errors;
            }
        };

        // running git directly seems the only sensible way to get the raw patch output. there is a programatic API but
        // it imposes hard limits on how much data can be returned and does not allow paging:
        // https://docs.atlassian.com/bitbucket-server/javadoc/5.8.0/api/reference/com/atlassian/bitbucket/commit/CommitService.html
        gitCommandBuilderFactory.builder(repo)
                .command("log").argument("-p").argument("-n").argument("1").argument(commitId)
                .build(outputHandler).call();
        return  (ArrayList<String>)   outputHandler.getOutput();

//        String[] banned = {
//                "password",
//                "12345678",
//                "aaaaaaaa",
//                "changeme",
//                "football",
//        };
//
//        ArrayList<String> errors = new ArrayList<>();
//
//        for (String s : outs) {
//            if (s.startsWith("+")) {
//                // new code added
//                for (String re : banned) {
//                    if (s.matches(re)) {
//                        errors.add("BANNED ---> " + s);
//                    }
//                }
//            }
//        }
//    Commit c = commitService.getCommit(new CommitRequest.Builder(repo, commitId).build());
//    PagedIterable<Change> changes = new PagedIterable(new PageProvider() {
//        public Page<Change> get(PageRequest pageRequest) {
//            return commitService.getChanges(new ChangesRequest.Builder(repo, commitId).build(), pageRequest);
//        }
//    }, 25);
//
//
//    for (Change change: changes) {
//        System.out.println("change > " +change.getSrcPath());
//    }


//            https://community.atlassian.com/t5/Bitbucket-questions/how-to-get-the-content-of-a-commit/qaq-p/100637
//    commitService.streamDiff(new DiffRequest.Builder(repo, commitId).build(), new DiffContentCallback() {
//        @Override
//        public void offerThreads(@Nonnull Stream<CommentThread> stream) throws IOException {
//            System.out.println("OT");
//        }
//
//        @Override
//        public void onBinary(@Nullable Path path, @Nullable Path path1) throws IOException {
//            System.out.println("OB");
//        }
//
//        @Override
//        public void onDiffEnd(boolean b) throws IOException {
//            System.out.println("ODE");
//        }
//
//        @Override
//        public void onDiffStart(@Nullable Path path, @Nullable Path path1) throws IOException {
//            System.out.println("ODS");
//        }
//
//        @Override
//        public void onEnd(@Nonnull DiffSummary diffSummary) throws IOException {
//            System.out.println("OE");
//            System.out.println(diffSummary.getResult().toString());
//            System.out.println(diffSummary.toString());
//            System.out.println("TRUNCATED? >> " +diffSummary.isTruncated());
//        }
//
//        @Override
//        public void onHunkEnd(boolean b) throws IOException {
//            System.out.println("OHE");
//        }
//
//        @Override
//        public void onSegmentEnd(boolean b) throws IOException {
//            System.out.println("OSE");
//
//        }
//
//        @Override
//        public void onSegmentLine(@Nonnull String s, @Nullable ConflictMarker conflictMarker, boolean b) throws IOException {
//            System.out.println("OSL");
//
//        }
//
//        @Override
//        public void onSegmentStart(@Nonnull DiffSegmentType diffSegmentType) throws IOException {
//            System.out.println("OSS");
//
//        }
//
//        @Override
//        public void onStart(@Nonnull DiffContext diffContext) throws IOException {
//            System.out.println("OS");
//
//        }
//        // ... implement methods that are called as the diff is streamed.
        // Note that the content will be capped after 10k lines across the change. You can get diffs of individual paths if you need to.
//    });
//        return errors;
    }
    

    @Nonnull
    @Override
    public RepositoryHookResult preUpdate(@Nonnull PreRepositoryHookContext preRepositoryHookContext, @Nonnull RepositoryPushHookRequest repositoryPushHookRequest) {
        PrintWriter out = repositoryPushHookRequest.getScmHookDetails().get().out();
        out.println(">>>>>>>>>>>>>>>>start of plugin output");
        RepositoryHookResult result = null;

        ArrayList<String> errors = new ArrayList<>();

        for (RefChange refChange : repositoryPushHookRequest.getRefChanges()) {
            String toHash = refChange.getToHash();
            out.println("got hash: " + toHash);
            errors.addAll(getItInParts(repositoryPushHookRequest.getRepository(), toHash));


        }

        if (errors.isEmpty()) {
            out.println("CommitScanner: ✓");
            result = RepositoryHookResult.accepted();
        } else {
            out.println("CommitScanner: ✘");
            for (String error : errors) {
                out.print(error);
            }
            result = RepositoryHookResult.rejected("***REJECTED***", "Regexps failed");
        }

        return result;
    }

}
