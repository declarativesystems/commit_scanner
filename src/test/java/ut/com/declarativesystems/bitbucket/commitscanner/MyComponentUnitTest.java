package ut.com.declarativesystems.bitbucket.commitscanner;

import org.junit.Test;
import com.declarativesystems.bitbucket.commitscanner.api.MyPluginComponent;
import com.declarativesystems.bitbucket.commitscanner.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}